﻿using UnityEngine;
using System.Collections;
using UnityEditor;

/** Editor setup for the GridManager script.
 * */
[CustomEditor(typeof(GridManager))]
public class GridManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GridManager manager = (GridManager)target;
        if (GUILayout.Button("Clear Grid"))
        {
            manager.EditorClear();
        }
        if (GUILayout.Button("Fill Grid"))
        {
            manager.EditorFill();
        }
    }
}
