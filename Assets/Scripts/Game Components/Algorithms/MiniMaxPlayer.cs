﻿
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;


namespace MiniMax
{
    public struct Move
    {
        public Move(BoardSquareTeam inTeam, Vector2Int inFromPoint, Vector2Int inToPoint) {
            team = inTeam; fromPoint = inFromPoint; toPoint = inToPoint;
        }

        public BoardSquareTeam team;
        public Vector2Int fromPoint;
        public Vector2Int toPoint;

        public override string ToString()
        {
            return String.Concat("{ from:", fromPoint, ", to:", toPoint, " }");
        }
    }

    public class MiniMaxPlayer : AIPlayer
    {
        protected static readonly int maxDepth = 3;
        protected static readonly int minScore = -10000;
        protected static readonly int maxScore = 10000;
        protected static readonly Vector2Int centerPoint = new Vector2Int(4, 4);


        public override void OnNewTurn()
        {
            if (IsPlaying())
            {
                BoardData board = GameManager.Instance.GridManager.Controller.Board;
                if (board.GetTeamAt(GameManager.Instance.GoalSquare) == Team)
                {
                    GameManager.Instance.MovePieceAndFinishTurn(
                        GameManager.Instance.GoalSquare,
                        board.GetAvailableMoves(GameManager.Instance.GoalSquare)[0]);
                    return;
                }

                Profiler.BeginSample("Minimax");
                Move bestMove = FindBestMove();
                Profiler.EndSample();

                GameManager.Instance.MovePieceAndFinishTurn(bestMove.fromPoint, bestMove.toPoint);
            }
        }

        protected int EvaluateMove(BoardData board, ref Move move)
        {
            Profiler.BeginSample("Score");
            int score = 0;

            // Condition 1: center piece. If mine and my turn, I win. If not mine and not my turn, I lose
            if (board.GetAttitudeAt(move.team, centerPoint) == BoardSquareAttitude.Enemy)
            {
                Profiler.EndSample();
                return move.team == Team ? minScore : maxScore;
            }
            
            // Condition 2: count how many pieces menace the center.
            int myMenacingPieces = CountMenacingPieces(board, Team);
            int enemyMenacingPieces = CountMenacingPieces(board, BoardData.GetEnemy(Team));
            int menaceDifference = myMenacingPieces - enemyMenacingPieces;
            if (Team == move.team)
            {
                if (menaceDifference < 0) return minScore;
                else if (menaceDifference > 0) score += 100;
                else if (menaceDifference > 1) return maxScore;
                //score += menaceDifference * 100;
            }
            else
            {
                if (menaceDifference < -1) return minScore;
                else if (menaceDifference == 0) score += 100;
                else if (menaceDifference > 0) return maxScore;
                //score += menaceDifference * 100;
            }

            // Condition 3: number of pieces
            int pieceDifference = board.GetTeamPieceCount(BoardData.GetEnemy(Team)) - board.GetTeamPieceCount(Team);
            score += pieceDifference * 50;



            Profiler.EndSample();
            return score;
        }


        public virtual Move FindBestMove()
        {
            int bestScore = minScore;
            // Default move
            Move bestMove = new Move(BoardSquareTeam.Empty, Vector2Int.Zero, Vector2Int.Zero);

            Move move = new Move(BoardData.GetEnemy(Team), Vector2Int.Zero, Vector2Int.Zero);
            ForEachPossibleMove(GameManager.Instance.GridManager.Controller.Board, ref move, (childBoard, childMove) =>
            {
                int newScore = Minimax(childBoard, childMove, maxDepth - 1);
                if (bestScore < newScore) // Max
                {
                    bestScore = newScore;
                    bestMove = childMove;
                }
                return false;
            });
            
            return bestMove;
        }
        
        int Minimax(BoardData board, Move move, int depth) {
            if (depth <= 0 || IsWinMove(ref move))
            {
                return EvaluateMove(board, ref move);
            }

            // For each piece of the same team, test all its available moves
            // Complexity OMin(1*8) OMax(9*8)

            BoardSquareTeam enemyTeam = BoardData.GetEnemy(move.team);
            int nextDepth = depth - 1;

            if (Team != move.team)
            {
                int score = minScore;
                ForEachPossibleMove(board, ref move, (childBoard, childMove) =>
                {
                    int newScore = Minimax(childBoard, childMove, nextDepth);
                    if (score < newScore) // Max
                        score = newScore;
                    return false;
                });
                return score;
            }
            else
            {
                int score = maxScore;
                ForEachPossibleMove(board, ref move, (childBoard, childMove) =>
                {
                    int newScore = Minimax(childBoard, childMove, nextDepth);
                    if (score > newScore) // Min
                        score = newScore;
                    return false;
                });
                return score;
            }
        }

        protected delegate bool OnPossibleMove(BoardData board, Move move);
        protected void ForEachPossibleMove(BoardData board, ref Move move, OnPossibleMove Callback)
        {
            BoardSquareTeam enemyTeam = BoardData.GetEnemy(move.team);
            
            for (int y = 0; y < 9; ++y)
            {
                for (int x = 0; x < 9; ++x)
                {
                    Vector2Int squareCoord = new Vector2Int(x, y);
                    BoardSquareTeam foundTeam = board.GetTeamAt(squareCoord);

                    if (foundTeam == enemyTeam)
                    {
                        List<Vector2Int> moves = board.GetAvailableMoves(squareCoord);
                        foreach (Vector2Int moveTarget in moves)
                        {
                            BoardData moveBoard = board.Clone();
                            moveBoard.MovePiece(squareCoord, moveTarget);

                            if (Callback(moveBoard, new Move(enemyTeam, squareCoord, moveTarget)))
                                break;
                        }
                    }
                }
            }
        }


        protected bool IsWinMove(ref Move move)
        {
            return move.fromPoint == centerPoint;
        }

        protected static List<Vector2Int> PointsNearCenter { get {
            return GameManager.Instance.GridManager.Controller.PointsNearCenter;
        }}

        protected int CountMenacingPieces(BoardData board, BoardSquareTeam team)
        {
            int count = 0;
            foreach (Vector2Int point in PointsNearCenter)
            {
                if (board.GetTeamAt(point) == team) ++count;
            }
            return count;
        }
    }
}
