﻿

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

namespace MiniMax
{
    public class AlphaBetaPlayer : MiniMaxPlayer
    {
        public override Move FindBestMove()
        {
            // Minus 1 to avoid no move movement
            int bestScore = minScore - 1;
            // Default move
            List<Move> bestMoves = new List<Move>();

            Move move = new Move(BoardData.GetEnemy(Team), Vector2Int.Zero, Vector2Int.Zero);
            ForEachPossibleMove(GameManager.Instance.GridManager.Controller.Board, ref move, (childBoard, childMove) =>
            {
                int newScore = Minimax(childBoard, childMove, minScore, maxScore, maxDepth - 1);
                Debug.Log("Posible move: " + childMove + " Value:" + newScore);

                if (bestScore == newScore)
                {
                    bestMoves.Add(childMove);
                }
                else if (bestScore < newScore) // Max
                {
                    bestScore = newScore;
                    bestMoves.Clear();
                    bestMoves.Add(childMove);
                }

                return false;
            });

            Debug.Log(name + ": My best move is " + bestMoves);

            // Random choose the move to return
            return bestMoves[Random.Range(0, bestMoves.Count - 1)];
        }

        int Minimax(BoardData board, Move move, int alpha, int beta, int depth)
        {
            if (depth <= 0 || IsWinMove(ref move))
            {
                return EvaluateMove(board, ref move);
            }

            // For each piece of the same team, test all its available moves
            // Complexity OMin(1*8) OMax(9*8)

            BoardSquareTeam enemyTeam = BoardData.GetEnemy(move.team);
            int nextDepth = depth - 1;

            if (Team != move.team)
            {
                Profiler.BeginSample("Max");
                int score = minScore;
                ForEachPossibleMove(board, ref move, (childBoard, childMove) =>
                {
                    int newScore = Minimax(childBoard, childMove, alpha, beta, nextDepth);
                    if (score < newScore) // Max Score
                        score = newScore;

                    if (alpha < newScore) // Max Alpha
                        alpha = newScore;

                    return beta <= alpha; // Break For-Each
                });
                Profiler.EndSample();
                return score;
            }
            else
            {
                Profiler.BeginSample("Min");
                int score = maxScore;
                ForEachPossibleMove(board, ref move, (childBoard, childMove) =>
                {
                    int newScore = Minimax(childBoard, childMove, alpha, beta, nextDepth);
                    if (score > newScore) // Min Score
                        score = newScore;

                    if (beta > newScore) // Max Alpha
                        beta = newScore;

                    return beta <= alpha; // Break For-Each
                });
                Profiler.EndSample();
                return score;
            }
        }
    }
}
