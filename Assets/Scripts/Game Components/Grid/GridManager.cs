﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour {

    GridController gridController;
    public GridController Controller { get { return gridController; } }

    [SerializeField]
    Vector2 squareSize;

    [SerializeField]
    GamePiece gamePiecePrefab;

    void Start()
    {
        gridController = GetComponent<GridController>();
    }

    private GamePiece CreatePiece(Vector2Int position) { return CreatePiece(position.x, position.y); }
    private GamePiece CreatePiece(int row, int column)
    {
        GamePiece obj = Instantiate(gamePiecePrefab, transform);
        obj.gameObject.name += string.Concat("[", row, "][", column, "]");
        obj.Position = new Vector2Int(row, column);
        return obj;
    }

    /** Transforms the given position of the grid to the center of the square to world location.
     * */
    public Vector3 SquareCenterToWorldLocation(Vector2Int squarePosition)
    {
        return ToWorldLocation(squarePosition) + new Vector3(0.5f * squareSize.x, 0f, 0.5f * squareSize.y);
    }

    /** Transforms the given position of the grid to world location.
     * */
    public Vector3 ToWorldLocation(Vector2Int position)
    {
        float xOffset = squareSize.x * position.x;
        float zOffset = squareSize.y * position.y;

        return transform.position + new Vector3(xOffset, 0f, zOffset);
    }

    public void Clear()
    {
        for(int i = 0; i < transform.childCount; ++i)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    #region EDITOR
    /** Editor function. Fills the grid with the given prefab.
     * */
    [ContextMenu("Fill Grid")]
    public void EditorFill()
    {
        GameObject[][] grid = new GameObject[9][];

        for(int row = 0; row < 9; ++row)
        {
            grid[row] = new GameObject[9];

            for(int column = 0; column < 9; ++column)
            {
                CreatePiece(row, column);
            }
        }
    }

    /** Editor function. Erases all elements from the grid.
     * */
    [ContextMenu("Clear Grid")]
    public void EditorClear()
    {
        while(transform.childCount > 0)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }

    /**
     * Used to draw helper gizmos in the editor.
     * */
    void OnDrawGizmos()
    {
        // Debug.Log("OnDrawGizmos for " + name);

        for (int i = 0; i < 9 + 1; ++i)
        {
            Gizmos.DrawLine(
                ToWorldLocation(new Vector2Int(i, 0)),
                ToWorldLocation(new Vector2Int(i, 9)));
        }

        for (int i = 0; i < 9 + 1; ++i)
        {
            Gizmos.DrawLine(
                ToWorldLocation(new Vector2Int(0, i)),
                ToWorldLocation(new Vector2Int(9, i)));
        }

        Vector2Int gridPos = new Vector2Int();

        for (int i = 0; i < 9; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                gridPos.x = i;
                gridPos.y = j;

                Gizmos.DrawCube(
                    SquareCenterToWorldLocation(gridPos),
                    new Vector3(0.05f, 0.05f, 0.05f));
            }
        }
    }

    void DrawSquareGizmo(Vector2Int gridPosition)
    {
        Gizmos.DrawCube(
            SquareCenterToWorldLocation(gridPosition),
            new Vector3(
                squareSize.x - 0.1f,
                0.1f ,
                squareSize.y - 0.1f));
    }

    #endregion
}
