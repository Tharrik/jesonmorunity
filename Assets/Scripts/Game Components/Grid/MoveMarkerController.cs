﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveMarkerController : MonoBehaviour {

    #region Attributes

    // Piece that moves
    public GamePiece piece;

    // Piece that is menaced, if there is one.
    public GamePiece enemyPiece;

    // Target position of the move.
    Vector2Int gridPosition;

    #endregion

    #region Class Methods

    /** Creates a move marker in the given position.
     * */
    public static MoveMarkerController Constructor(GameObject prefab, GamePiece gamePiece, Vector2Int gridPosition, Transform parent)
    {
        GameObject obj = Instantiate(prefab);
        obj.transform.position = GameManager.Instance.GridManager.SquareCenterToWorldLocation(gridPosition);
        obj.transform.localScale = new Vector3(
            parent.localScale.x * obj.transform.localScale.x,
            parent.localScale.y * obj.transform.localScale.y,
            parent.localScale.z * obj.transform.localScale.z);

        MoveMarkerController square = obj.GetComponent<MoveMarkerController>();
        square.piece = gamePiece;
        square.gridPosition = gridPosition;

        return square;
    }

    #endregion

    /** Called when the mouse clicks over the collider if its the first collider in order.
     * */
    private void OnMouseDown()
    {
        GameManager.Instance.MovePieceAndFinishTurn(piece.Position, gridPosition);
    }

}
