﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Profiling;

public enum BoardSquareTeam { Empty, White, Black }
public enum BoardSquareAttitude { Friendly, Enemy, Neutral, Invalid }

public class TeamInfo
{
    public int piecesCount;

    public TeamInfo(int inPiecesCount) {
        piecesCount = inPiecesCount;
    }

    public TeamInfo(TeamInfo other)
    {
        piecesCount = other.piecesCount;
    }
}

public class BoardData
{
    public BoardSquareTeam[,] squares = new BoardSquareTeam[9, 9];
    TeamInfo whiteTeamInfo = new TeamInfo(9);
    TeamInfo blackTeamInfo = new TeamInfo(9);


    BoardData(BoardData other)
    {
        squares = (BoardSquareTeam[,])other.squares.Clone();
        whiteTeamInfo = new TeamInfo(other.whiteTeamInfo);
        blackTeamInfo = new TeamInfo(other.blackTeamInfo);
    }

    /** Generate data from GridController */
    public BoardData(GridController controller) {

        foreach (GamePiece piece in controller.WhitePieces)
        {
            squares[piece.Position.x, piece.Position.y] = BoardSquareTeam.White;
            whiteTeamInfo.piecesCount += 1;
        }

        foreach (GamePiece piece in controller.BlackPieces)
        {
            squares[piece.Position.x, piece.Position.y] = BoardSquareTeam.Black;
            blackTeamInfo.piecesCount += 1;
        }
    }

    public bool MovePiece(Vector2Int fromPosition, Vector2Int toPosition)
    {

        BoardSquareTeam pieceTeam = GetTeamAt(fromPosition);
        if (pieceTeam == BoardSquareTeam.Empty)
            return false;

        BoardSquareTeam targetTeam = GetTeamAt(toPosition);
        
        // Has eaten?
        if (targetTeam != BoardSquareTeam.Empty && targetTeam != pieceTeam)
            GetTeamInfo(targetTeam).piecesCount -= 1;

        if (pieceTeam != BoardSquareTeam.Empty)
        {
            SetTeamAt(fromPosition, BoardSquareTeam.Empty);
            SetTeamAt(toPosition, pieceTeam);
            return true;
        }
        return false;
    }

    public void DestroyPiece(BoardSquareTeam pieceTeam, Vector2Int point)
    {
        if (GetTeamAt(point) == pieceTeam)
        {
            GetTeamInfo(pieceTeam).piecesCount -= 1;
            SetTeamAt(point, BoardSquareTeam.Empty);
        }
    }

    /** If it exists, returns the piece in the given position.
     * Can be used in simulation
     */
    public BoardSquareTeam GetTeamAt(Vector2Int point)
    {
        return squares[point.x, point.y];
    }
    
    void SetTeamAt(Vector2Int point, BoardSquareTeam team)
    {
        squares[point.x, point.y] = team;
    }


    public BoardSquareAttitude GetAttitudeAt(BoardSquareTeam team, Vector2Int point)
    {
        if (!IsValidPoint(point))
            return BoardSquareAttitude.Invalid;

        return GetAttitude(GetTeamAt(point), team);
    }

    public bool CanMoveTo(BoardSquareTeam team, Vector2Int point)
    {
        return GetAttitudeAt(team, point) != BoardSquareAttitude.Friendly;
    }

    public bool IsValidPoint(Vector2Int point)
    {
        return point.IsInBounds(0, 0, 9 - 1, 9 - 1);
    }

    public List<Vector2Int> GetAvailableMoves(Vector2Int origin)
    {
        List<Vector2Int> points = new List<Vector2Int>();
        foreach (Vector2Int move in GameManager.Instance.pieceMoves)
        {
            Vector2Int targetPoint = origin.Add(move);
            BoardSquareAttitude attitude = GetAttitudeAt(GetTeamAt(origin), targetPoint);

            if (attitude == BoardSquareAttitude.Neutral || attitude == BoardSquareAttitude.Enemy)
            {
                points.Add(targetPoint);
            }
        }

        return points;
    }

    TeamInfo GetTeamInfo(BoardSquareTeam team)
    {
        switch (team)
        {
            case BoardSquareTeam.White:
                return whiteTeamInfo;
            case BoardSquareTeam.Black:
                return blackTeamInfo;
            default:
                return null;
        }
    }

    public int GetTeamPieceCount(BoardSquareTeam team) {
        return team != BoardSquareTeam.Empty? GetTeamInfo(team).piecesCount : -1;
    }


    static public BoardSquareAttitude GetAttitude(BoardSquareTeam A, BoardSquareTeam B)
    {
        if (A == BoardSquareTeam.Empty || B == BoardSquareTeam.Empty)
            return BoardSquareAttitude.Neutral;

        return A == B ? BoardSquareAttitude.Friendly : BoardSquareAttitude.Enemy;
    }

    static public BoardSquareTeam GetEnemy(BoardSquareTeam team)
    {
        switch (team) {
            case BoardSquareTeam.Black:
                return BoardSquareTeam.White;
            case BoardSquareTeam.White:
                return BoardSquareTeam.Black;
            default:
                return BoardSquareTeam.Empty;
        }
    }

    #region CLONE INTERFACE

    public BoardData Clone()
    {
        return new BoardData(this);
    }

    #endregion
}
