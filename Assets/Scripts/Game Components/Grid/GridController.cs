﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridController : MonoBehaviour {

    private GridManager gridManager;
    /** Used to store active board state. Gets synchronized when any piece changes by the grid controller */
    private BoardData board;
    public BoardData Board { get { return board; } }

    [SerializeField]
    private GameObject freeSquarePrefab, enemySquarePrefab;
    
    private List<GamePiece> whitePieces, blackPieces;

    public List<GamePiece> WhitePieces { get { return whitePieces; } }
    public List<GamePiece> BlackPieces { get { return blackPieces; } }

    private List<MoveMarkerController> squareMarkers;


    [SerializeField]
    List<Vector2Int> pointsNearCenter;
    public List<Vector2Int> PointsNearCenter { get { return pointsNearCenter; } }


    void Start()
    {
        gridManager = GetComponent<GridManager>();
        squareMarkers = new List<MoveMarkerController>();
        GetPieces();
        board = new BoardData(this);
    }

    /** Fills the piece lists. Used when instantiated to get the piece references.
     * */
    private void GetPieces()
    {
        whitePieces = new List<GamePiece>();
        blackPieces = new List<GamePiece>();

        GamePiece[] pieces = GetComponentsInChildren<GamePiece>();
        foreach (GamePiece piece in pieces)
        {
            if (piece.IsWhite)
            {
                piece.name += " WHITE";
                whitePieces.Add(piece);
            }
            else
            {
                piece.name += " BLACK";
                blackPieces.Add(piece);
            }
        }
    }

    public bool MovePiece(Vector2Int fromPosition, Vector2Int toPosition)
    {
        // Cache pieces
        GamePiece piece = GetPieceAt(fromPosition);
        GamePiece enemyPiece = GetPieceAt(toPosition);

        if (board.MovePiece(fromPosition, toPosition))
        {
            if (piece)
                piece.Position = toPosition;

            if (enemyPiece)
                DeletePiece(enemyPiece);
            return true;
        }
        return false;
    }

    public void DeletePiece(GamePiece piece)
    {
        if (piece.IsWhite)
            whitePieces.Remove(piece);
        else
            blackPieces.Remove(piece);

        board.DestroyPiece(piece.Team, piece.Position);

        Destroy(piece.gameObject);
    }

    /** Shows the possible moves of the given piece in the board.
     * */
    public void ShowMoves(GamePiece piece)
    {
        List<Vector2Int> moves = board.GetAvailableMoves(piece.Position);
        foreach (Vector2Int move in moves)
        {
            switch (board.GetAttitudeAt(piece.Team, move))
            {
                case BoardSquareAttitude.Neutral:
                    squareMarkers.Add(MoveMarkerController.Constructor(freeSquarePrefab, piece, move, gameObject.transform));
                    break;
                case BoardSquareAttitude.Enemy:
                    MoveMarkerController enemyMarker = MoveMarkerController.Constructor(enemySquarePrefab, piece, move, gameObject.transform);
                    enemyMarker.enemyPiece = GetPieceAt(move);
                    squareMarkers.Add(enemyMarker);
                    break;
            }
        }
    }

    /** If it exists, returns the piece in the given position.
     * */
    public GamePiece GetPieceAt(Vector2Int position)
    {
        // Fast check
        if (GetSquareStateAt(position) == BoardSquareTeam.Empty)
            return null;

        foreach(GamePiece piece in whitePieces)
        {
            if (piece.Position == position) return piece;
        }

        foreach (GamePiece piece in blackPieces)
        {
            if (piece.Position == position) return piece;
        }

        return null;
    }

    /** If it exists, returns the piece in the given position.
     * Can be used in simulation
     */
    public BoardSquareTeam GetSquareStateAt(Vector2Int position)
    {
        return board.GetTeamAt(position);
    }

    public void ClearMoves()
    {
        foreach(MoveMarkerController square in squareMarkers)
        {
            Destroy(square.gameObject);
        }

        squareMarkers.Clear();
    }


}
