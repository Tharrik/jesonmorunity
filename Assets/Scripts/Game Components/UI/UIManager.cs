﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    [SerializeField]
    UITimer timer;
    [SerializeField]
    UITurnIndicator[] playerIndicators;
    [SerializeField]
    Text messageBox;
    [SerializeField]
    Button playButton;
    [SerializeField]
    Button resetButton;

    // Use this for initialization
    void Start () {
        
	}
	
    public void OnBegin()
    {
        // Hide UI elements
        messageBox.enabled = false;
        Disable(playButton);

        // Start timer
        timer.StartTimer();
    }

    public void OnNewTurn()
    {
        foreach(UITurnIndicator indicator in playerIndicators)
        {
            indicator.OnNewTurn();
        }
    }

    public void OnReady()
    {
        // Hide UI elements
        Disable(resetButton);

        // Show UI elements
        messageBox.enabled = true;
        messageBox.text = "Ready to play!";
        Enable(playButton);

        // Reset timer
        timer.ResetTimer();
    }

    public void OnEndGame()
    {
        // Show UI elements
        messageBox.enabled = true;
        messageBox.text = GameManager.Instance.ActivePlayer.name + " wins!";
        Enable(resetButton);

        // Stop timer
        timer.StopTimer();
    }

    private void Enable(Button button)
    {
        button.gameObject.SetActive(true);
    }

    private void Disable(Button button)
    {
        button.gameObject.SetActive(false);
    }
}
