﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UITimer : MonoBehaviour {

    float time, initialTime;
    Text text;

    IEnumerator timerCoroutine;

    private void Start()
    {
        text = GetComponent<Text>();
        text.font.material.mainTexture.filterMode = FilterMode.Point;
    }

    public void StartTimer()
    {
        initialTime = Time.time;
        StartCoroutine(UpdateCoroutine(1f));
    }

    public void StopTimer()
    {
        StopCoroutine(timerCoroutine);
    }

    public void ResetTimer()
    {
        if(text != null)
        text.text = TimeFloatToString(0f);
    }

    IEnumerator UpdateCoroutine(float delay)
    {
        yield return new WaitForSeconds(delay);

        time = Time.time - initialTime;
        text.text = TimeFloatToString(time);

        float newDelay = Mathf.Ceil(time) - time;
        timerCoroutine = UpdateCoroutine(newDelay);

        StartCoroutine(timerCoroutine);
    }

    private string TimeFloatToString(float time)
    {
        int minutes = (int)time / 60;
        int seconds = (int)time % 60;
        return string.Concat(minutes.ToString("00"), ":", seconds.ToString("00"));
    }
}
