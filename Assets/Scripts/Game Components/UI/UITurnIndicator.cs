﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class UITurnIndicator : MonoBehaviour {

    Image image;
    [SerializeField]
    BoardSquareTeam team;

	// Use this for initialization
	void Start () {
        image = GetComponent<Image>();
	}

    public void OnNewTurn()
    {
        //Debug.Log(name + ": On New Turn!");
        if (GameManager.Instance.IsActivePlayer(GameManager.Instance.GetPlayer(team))) {
            Activate();
        }
        else {
            Deactivate();
        }
    }

	private void Activate()
    {
        image.enabled = true;
    }

    private void Deactivate()
    {
        image.enabled = false;
    }
}
