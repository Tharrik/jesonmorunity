﻿
using UnityEngine;

public class BasePlayer : MonoBehaviour {

    /** Returns the team of this player */
    public BoardSquareTeam Team {
        get {
            if (GameManager.Instance.whitePlayer == this)
                return BoardSquareTeam.White;
            else if (GameManager.Instance.blackPlayer == this)
                return BoardSquareTeam.Black;
            return BoardSquareTeam.Empty;
        }
    }

    public virtual void OnNewTurn() { }

    public bool IsPlaying() { return GameManager.Instance.IsActivePlayer(this); }
}
