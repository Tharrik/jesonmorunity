﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum GameState { Null, Ready, Playing, End }

public class GameManager : MonoBehaviour {

	[SerializeField]
	private GridManager grid;   // Remember to recatch the new grid when reseting
	public GridManager GridManager { get { return grid; } }
	[SerializeField]
	private GameObject gridPrefab;

	[SerializeField]
	private UIManager UI;

	public Vector2Int[] pieceMoves;

	[SerializeField]
	private Vector2Int goalSquare;
	public Vector2Int GoalSquare { get { return goalSquare; } }

	private GameState gameState = GameState.Null;


	[SerializeField]
	public BasePlayer whitePlayer, blackPlayer;
	private BasePlayer activePlayer;
	public BasePlayer ActivePlayer { get { return activePlayer; } }
	public BasePlayer[] AllPlayers { get {
			return new BasePlayer[2] { whitePlayer, blackPlayer };
	}}

	private static GameManager instance;
	public static GameManager Instance
	{
		get {
			return instance? instance : (instance = FindObjectOfType<GameManager>());
		}
	}

	#region UNITY EVENTS

	public UnityEvent onReady;
	public UnityEvent onBegin;
	public UnityEvent onNewTurn;
	public UnityEvent onEndGame;

	#endregion


	// Use this for initialization
	void Start () {
		if (GameManager.instance && GameManager.instance != this)
		{
			Destroy(this);
			return;
		}
		GameManager.instance = this;
		activePlayer = null;

		gameState = GameState.Ready;

		onReady.Invoke();
	}

	#region GAME FLOW METHODS

	/** Starts the game. Only calleable if GameState is Ready.
	 * */
	public void BeginPlay()
	{
		if (gameState != GameState.Ready)
			return;

		gameState = GameState.Playing;
		activePlayer = whitePlayer;
		NewTurn();

		// Debug.Log("NEW GAME ===========================");

		onBegin.Invoke();
	}

	public void MovePieceAndFinishTurn(Vector2Int fromPosition, Vector2Int toPosition)
	{
		// Check Victory
		bool winMove = fromPosition == GoalSquare;
		
		GridManager.Controller.MovePiece(fromPosition, toPosition);

		// Deselect piece
		Player player = activePlayer as Player;
		if (player)
			player.SetSelectedPiece(null);

		if (winMove)
			EndGame();
		else
			NewTurn();
	}

	/** Switches the active player */
	public void NewTurn()
	{
		// Change active player
		activePlayer = (activePlayer == whitePlayer)? blackPlayer : whitePlayer;
        Debug.Log(name + ": Turn for " + activePlayer);

		// Call on new turn event.
		onNewTurn.Invoke();

		// Tell the players to react to the new turn
		foreach (BasePlayer bPlayer in AllPlayers)
		{
			bPlayer.OnNewTurn();
		}
	}

	/** Ends the game
	 * */
	public void EndGame()
	{
		gameState = GameState.End;
		
		onEndGame.Invoke();
	}

	/** Resets the board.
	 * */
	public void Reset()
	{
		// Reset the grid
		grid.Clear();
		Destroy(grid.gameObject);
		grid = Instantiate(gridPrefab).GetComponent<GridManager>();
		
		// Update game state
		gameState = GameState.Ready;

		// Call OnReady Event
		onReady.Invoke();
	}

	#endregion

	#region PUBLIC METHODS

	public bool IsActivePlayer(BasePlayer player) { return player == activePlayer; }

	public BasePlayer GetPlayer(BoardSquareTeam team) {
		switch (team) {
			case BoardSquareTeam.White:
				return whitePlayer;
			case BoardSquareTeam.Black:
				return blackPlayer;
			default:
				return null;
		}
	}

	#endregion

	#region INTERNAL METHODS

	/** Checks if a move is a winning move. */
	bool CheckWin(Vector2Int origin) { return origin == goalSquare; }

	#endregion

	#region EDITOR
	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawCube(
			grid.SquareCenterToWorldLocation(goalSquare),
			new Vector3(0.2f, 0.05f, 0.2f));
	} 
	#endregion

}
