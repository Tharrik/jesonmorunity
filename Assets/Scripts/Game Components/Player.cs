﻿using UnityEngine;


public class Player : BasePlayer {
    private GamePiece selectedPiece;


    public void SetSelectedPiece(GamePiece piece) {
        if (selectedPiece)
        {
            selectedPiece.OnDeselected();
            GameManager.Instance.GridManager.Controller.ClearMoves();
        }

        selectedPiece = piece;

        if (selectedPiece)
        {
            selectedPiece.OnSelected();
            GameManager.Instance.GridManager.Controller.ShowMoves(selectedPiece);
        }
    }
}
