﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePiece : MonoBehaviour {

	[SerializeField]
	private BoardSquareTeam team;

	[SerializeField]
	private Vector2Int position;

	[SerializeField]
	private Material blackMat;
	[SerializeField]
	private Material whiteMat;


	public bool IsWhite { get {
		return team == BoardSquareTeam.White;
	}}

	public BoardSquareTeam Team { get {
		return team;
	}}

	public BasePlayer Owner { get {
		return GameManager.Instance.GetPlayer(team);
	}}

	public Vector2Int Position
	{
		get { return position; }
		set {
			position = value.Copy();
			transform.position = GameManager.Instance.GridManager.SquareCenterToWorldLocation(position);
		}
	}



	void OnDrawGizmos()
	{
		if (IsWhite)
		{
			// Apply Material for White Piece
			GetComponentInChildren<Renderer>().material = whiteMat;
		}
		else
		{
			// Apply Material for Black Piece
			GetComponentInChildren<Renderer>().material = blackMat;
		}
	}

	void OnMouseDown()
	{
		if (Owner is AIPlayer || !Owner.IsPlaying())
			return;

		//Debug.Log(name + ": Clicked!");
		((Player)Owner).SetSelectedPiece(this);
	}

	public void OnSelected()
	{
		GetComponentInChildren<Renderer>().material.EnableKeyword("_EMISSION");
	}

	public void OnDeselected()
	{
		// TODO: Cache Renderer
		GetComponentInChildren<Renderer>().material.DisableKeyword("_EMISSION");
	}
}
