﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

[System.Serializable]
public struct Vector2Int {
    public static readonly Vector2Int Zero = new Vector2Int(0, 0);

    public int x, y;
    
    public Vector2Int(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Vector2Int Copy()
    {
        return new Vector2Int(x, y);
    }
    
    public Vector2Int Add(Vector2Int other) { return new Vector2Int(this.x + other.x, this.y + other.y); }

    public static bool operator ==(Vector2Int a, Vector2Int b) { return a.x == b.x && a.y == b.y; ; }
    public static bool operator !=(Vector2Int a, Vector2Int b) { return !(a == b); }
    public override bool Equals(object other) { return this == (Vector2Int)other; }
    public override int GetHashCode() { return x ^ y; }

    public override string ToString()
    {
        return string.Concat("(", x, ", ", y, ")");
    }

    public bool IsInBounds(int xMin, int yMin, int xMax, int yMax) {
        return x >= xMin && x <= xMax && y >= yMin && y <= yMax;
    }
}
